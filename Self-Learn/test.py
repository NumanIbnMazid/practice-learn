# class Computer:
#     # pass
#     def __init__(self):
#         self.name = "Disha"
#         self.age = 15

# c1 = Computer()
# c2 = Computer()

# # print(id(c1))
# # print(id(c2))

# c2.name = "Numan"

# print(c1.name)
# print(c2.name)

# import urllib.request

# urllib.request.urlretrieve(
#     "https://images.pexels.com/photos/1010519/pexels-photo-1010519.jpeg?cs=srgb&dl=brown-and-black-cut-away-acoustic-guitar-1010519.jpg", "guitar.jpg"
# )

# from urllib.request import Request, urlopen

# req = Request('http://www.cmegroup.com/trading/products/#sortField=oi&sortAsc=false&venues=3&page=1&cleared=1&group=1',
#               headers={'User-Agent': 'Mozilla/5.0'})
# webpage = urlopen(req).read()


# import urllib.request
# resource = urllib.request.urlopen(
#     "http://www.digimouth.com/news/media/2011/09/google-logo.jpg")
# output = open("file01.jpg", "wb")
# output.write(resource.read())
# output.close()

# Lambda Function 

# full_name = lambda fn, ln: fn.strip().title() + " " + ln.strip().title()

# print(full_name("Numan", "Mazid"))